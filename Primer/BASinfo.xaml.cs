﻿using System;
using System.IO;
using System.Linq;
using System.Windows;
using MahApps.Metro.Controls;
using System.Collections.Generic;

namespace Primer
{
    /// <summary>
    /// Interaction logic for BASinfo.xaml
    /// </summary>
    public partial class BASinfo : MetroWindow
    {
        public BASinfo()
        {
            InitializeComponent();
        }

        private void ImportActivity_Click(object sender, RoutedEventArgs e)
        {
            importact();
        }

        private void importact()
        {
            // Configure open file dialog box
            Microsoft.Win32.OpenFileDialog opendialog = new Microsoft.Win32.OpenFileDialog();
            opendialog.FileName = ""; // Default file name
            opendialog.DefaultExt = "*.csv"; // Default file extension
            opendialog.Filter = "CSV files (.csv)|*.csv"; // Filter files by extension

            // Show open file dialog box
            Nullable<bool> result = opendialog.ShowDialog();

            // Process open file dialog box results


            if (result == true)
            {
                string FileName = opendialog.FileName;

                List<String> listStrLineElements = new List<string>();
                List<string> filepanel = new List<string>();

                try
                {
                    using (StreamReader sr = new StreamReader(FileName))
                    {
                        String line;
                        while ((line = sr.ReadLine()) != null)
                        {




                            listStrLineElements.Add(line);
                        }
                    }


                    foreach (string li in listStrLineElements)
                    {
                        string lis = li.Replace(",", " ");
                        string liss = lis.Replace(@"""", " ");

                        if (liss.Contains(@"Field Panel has"))
                        {


                            filepanel.Add(liss);
                        }
                    }
                    Listboxactive.ItemsSource = filepanel;
                }



                catch (Exception EX)
                {
                    MessageBox.Show(EX.ToString());
                }
            }
        }

        private void PPCLchanges_Click(object sender, RoutedEventArgs e)
        {

            {
                // Configure open file dialog box
                Microsoft.Win32.OpenFileDialog opendialog = new Microsoft.Win32.OpenFileDialog();
                opendialog.FileName = ""; // Default file name
                opendialog.DefaultExt = "*.csv"; // Default file extension
                opendialog.Filter = "CSV files (.csv)|*.csv"; // Filter files by extension

                // Show open file dialog box
                Nullable<bool> result = opendialog.ShowDialog();

                // Process open file dialog box results


                if (result == true)
                {
                    string FileName = opendialog.FileName;

                    List<String> listStrLineElements = new List<string>();
                    List<string> filepanel = new List<string>();

                    try
                    {
                        using (StreamReader sr = new StreamReader(FileName))
                        {
                            String line;
                            while ((line = sr.ReadLine()) != null)
                            {




                                listStrLineElements.Add(line);
                            }
                        }


                        foreach (string li in listStrLineElements)
                        {
                            string lis = li.Replace(",", " ");
                            string liss = lis.Replace(@"""", " ");

                            if (liss.Contains(@"PPCL was"))
                            {


                                filepanel.Add(liss);
                            }
                        }
                        Listboxactive.ItemsSource = filepanel;
                    }



                    catch (Exception EX)
                    {
                        MessageBox.Show(EX.ToString());
                    }
                }
            }
        }

        private void Commanded_Click(object sender, RoutedEventArgs e)
        {
            {
                // Configure open file dialog box
                Microsoft.Win32.OpenFileDialog opendialog = new Microsoft.Win32.OpenFileDialog();
                opendialog.FileName = ""; // Default file name
                opendialog.DefaultExt = "*.csv"; // Default file extension
                opendialog.Filter = "CSV files (.csv)|*.csv"; // Filter files by extension

                // Show open file dialog box
                Nullable<bool> result = opendialog.ShowDialog();

                // Process open file dialog box results


                if (result == true)
                {
                    string FileName = opendialog.FileName;

                    List<String> listStrLineElements = new List<string>();
                    List<string> filepanel = new List<string>();

                    try
                    {
                        using (StreamReader sr = new StreamReader(FileName))
                        {
                            String line;
                            while ((line = sr.ReadLine()) != null)
                            {




                                listStrLineElements.Add(line);
                            }
                        }


                        foreach (string li in listStrLineElements)
                        {
                            string lis = li.Replace(",", " ");
                            string liss = lis.Replace(@"""", " ");

                            if (liss.Contains(@"Commanded"))
                            {


                                filepanel.Add(liss);
                            }
                        }
                        Listboxactive.ItemsSource = filepanel;
                    }



                    catch (Exception EX)
                    {
                        MessageBox.Show(EX.ToString());
                    }
                }
            }
        }

        private void Graphics_ContextMenuClosing(object sender, System.Windows.Controls.ContextMenuEventArgs e)
        {
            {
                // Configure open file dialog box
                Microsoft.Win32.OpenFileDialog opendialog = new Microsoft.Win32.OpenFileDialog();
                opendialog.FileName = ""; // Default file name
                opendialog.DefaultExt = "*.csv"; // Default file extension
                opendialog.Filter = "CSV files (.csv)|*.csv"; // Filter files by extension

                // Show open file dialog box
                Nullable<bool> result = opendialog.ShowDialog();

                // Process open file dialog box results


                if (result == true)
                {
                    string FileName = opendialog.FileName;

                    List<String> listStrLineElements = new List<string>();
                    List<string> filepanel = new List<string>();

                    try
                    {
                        using (StreamReader sr = new StreamReader(FileName))
                        {
                            String line;
                            while ((line = sr.ReadLine()) != null)
                            {




                                listStrLineElements.Add(line);
                            }
                        }


                        foreach (string li in listStrLineElements)
                        {
                            string lis = li.Replace(",", " ");
                            string liss = lis.Replace(@"""", " ");

                            if (liss.Contains(@"Graphics was"))
                            {


                                filepanel.Add(liss);
                            }
                        }
                        Listboxactive.ItemsSource = filepanel;
                    }



                    catch (Exception EX)
                    {
                        MessageBox.Show(EX.ToString());
                    }
                }
            }
        }

        private void Coldstarts_Click(object sender, RoutedEventArgs e)
        {
            {
                // Configure open file dialog box
                Microsoft.Win32.OpenFileDialog opendialog = new Microsoft.Win32.OpenFileDialog();
                opendialog.FileName = ""; // Default file name
                opendialog.DefaultExt = "*.csv"; // Default file extension
                opendialog.Filter = "CSV files (.csv)|*.csv"; // Filter files by extension

                // Show open file dialog box
                Nullable<bool> result = opendialog.ShowDialog();

                // Process open file dialog box results


                if (result == true)
                {
                    string FileName = opendialog.FileName;

                    List<String> listStrLineElements = new List<string>();
                    List<string> filepanel = new List<string>();

                    try
                    {
                        using (StreamReader sr = new StreamReader(FileName))
                        {
                            String line;
                            while ((line = sr.ReadLine()) != null)
                            {




                                listStrLineElements.Add(line);
                            }
                        }


                        foreach (string li in listStrLineElements)
                        {
                            string lis = li.Replace(",", " ");
                            string liss = lis.Replace(@"""", " ");

                            if (liss.Contains(@"Manual coldstart"))
                            {


                                filepanel.Add(liss);
                            }
                        }
                        Listboxactive.ItemsSource = filepanel;
                    }



                    catch (Exception EX)
                    {
                        MessageBox.Show(EX.ToString());
                    }
                }
            }
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.SaveFileDialog opendialog = new Microsoft.Win32.SaveFileDialog();
            opendialog.FileName = ""; // Default file name
            opendialog.DefaultExt = ".rtf"; // Default file extension
            opendialog.Filter = "RTF files (.rtf)|*.txt"; // Filter files by extension

            // Show open file dialog box
            Nullable<bool> result = opendialog.ShowDialog();

            // Process open file dialog box results
            if (result == true)
            {
                string fileName = opendialog.FileName;

                using (var sw = new StreamWriter(fileName, false))
                    foreach (var item in Listboxactive.Items)
                        sw.Write(item.ToString() + Environment.NewLine);

                if (MessageBox.Show("Successfully created file! Would you like to open it now?", "Question", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    System.Diagnostics.Process.Start(fileName);
                }


            }
        }

        private void Graphics1_Click(object sender, RoutedEventArgs e)
        {
            {
                // Configure open file dialog box
                Microsoft.Win32.OpenFileDialog opendialog = new Microsoft.Win32.OpenFileDialog();
                opendialog.FileName = ""; // Default file name
                opendialog.DefaultExt = "*.csv"; // Default file extension
                opendialog.Filter = "CSV files (.csv)|*.csv"; // Filter files by extension

                // Show open file dialog box
                Nullable<bool> result = opendialog.ShowDialog();

                // Process open file dialog box results


                if (result == true)
                {
                    string FileName = opendialog.FileName;

                    List<String> listStrLineElements = new List<string>();
                    List<string> filepanel = new List<string>();

                    try
                    {
                        using (StreamReader sr = new StreamReader(FileName))
                        {
                            String line;
                            while ((line = sr.ReadLine()) != null)
                            {




                                listStrLineElements.Add(line);
                            }
                        }


                        foreach (string li in listStrLineElements)
                        {
                            string lis = li.Replace(",", " ");
                            string liss = lis.Replace(@"""", " ");

                            if (liss.Contains(@"Graphics was"))
                            {


                                filepanel.Add(liss);
                            }
                        }
                        Listboxactive.ItemsSource = filepanel;
                    }



                    catch (Exception EX)
                    {
                        MessageBox.Show(EX.ToString());
                    }
                }
            }
        }
    }
}
