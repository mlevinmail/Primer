﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using System.Text.RegularExpressions;
using System.IO;


namespace Primer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        string FileName;
        string PPCL;

        private void Import1_Click(object sender, RoutedEventArgs e)
        {
            imprt();
        }


        private void imprt()
        {
            // Configure open file dialog box
            Microsoft.Win32.OpenFileDialog opendialog = new Microsoft.Win32.OpenFileDialog();
            opendialog.FileName = ""; // Default file name
            opendialog.DefaultExt = "*.txt"; // Default file extension
            opendialog.Filter = "Text files (.txt)|*.txt|CSV files (.csv)|*.csv"; // Filter files by extension

            // Show open file dialog box
            Nullable<bool> result = opendialog.ShowDialog();

            // Process open file dialog box results


            if (result == true)
            {
                string FileName = opendialog.FileName;
                string ppcl = File.ReadAllText(FileName);
                Text1filepath.Text = FileName;
                string replace = ppcl.Replace("\"", "");
                Import.Text = replace;
            }
        }

        private void Clear1_Click(object sender, RoutedEventArgs e)
        {
            Import.Text = "";
            Text1filepath.Text = "";
        }

        private void TraceBits_Click(object sender, RoutedEventArgs e)
        {
            string r = Import.Text;
            var output = Regex.Replace(r, @"\bET,", " ");
            var output2 = Regex.Replace(output, @"\bE,", " ");
            var output3 = Regex.Replace(output2, @"\bETF,", " ");
            var output4 = Regex.Replace(output3, @"\bE F,", " ");
            var output5 = Regex.Replace(output4, @"\bD,", " ");
            var output6 = Regex.Replace(output5, @"\bD U,", " ");
            var output7 = Regex.Replace(output6, @"\bE U,", " ");
            var output8 = Regex.Replace(output7, @"\bETU,", " ");

            string output9 = output8.Replace(",", "");
            Import.Text = output9;
        }

        private void SavePPCL1_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.SaveFileDialog savedialog = new Microsoft.Win32.SaveFileDialog();
            savedialog.FileName = ""; // Default file name
            savedialog.DefaultExt = ".txt"; // Default file extension
            savedialog.Filter = "Text Files (.txt)|*.txt| Rich Text files(.rtf) | *.rtf "; // Filter files by extension

            // Show open file dialog box
            Nullable<bool> result = savedialog.ShowDialog();

            // Process open file dialog box results
            if (result == true)
            {

                string saved = savedialog.FileName;
                File.WriteAllText(saved, Import.Text);
                if (MessageBox.Show("Successfully created file! Would you like to open it now?", "Question", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    System.Diagnostics.Process.Start(saved);
                }


            }
        }

        private void Import2_Click(object sender, RoutedEventArgs e)
        {
            // Configure open file dialog box
            Microsoft.Win32.OpenFileDialog opendialog = new Microsoft.Win32.OpenFileDialog();
            opendialog.FileName = ""; // Default file name
            opendialog.DefaultExt = "*.txt"; // Default file extension
            opendialog.Filter = "Text files (.txt)|*.txt|CSV files (.csv)|*.csv"; // Filter files by extension

            // Show open file dialog box
            Nullable<bool> result = opendialog.ShowDialog();

            // Process open file dialog box results


            if (result == true)
            {
                string FileName = opendialog.FileName;
                string ppcl = File.ReadAllText(FileName);
                Text2filepath.Text = FileName;
                string replace = ppcl.Replace("\"", "");
                Import3.Text = replace;
            }

        }

        private void TraceBits2_Click(object sender, RoutedEventArgs e)
        {
            string r = Import3.Text;
            //string remove1 = r.Replace(@"ET,", "");
            //string remove2 = remove1.Replace("E,", "");
            //string remove3 = remove2.Replace("E U", "");
            //string remove4 = remove3.Replace("ETU", "");
            //string remove5 = remove4.Replace("E F", "");
            //Import3.Text = remove2;
            //string[] prefixes = { "ET", "E", "ETF", "E F", "D", "D U", "E U", "ETU" };

            var output = Regex.Replace(r, @"\bET,", " ");
            var output2 = Regex.Replace(output, @"\bE,", " ");
            var output3 = Regex.Replace(output2, @"\bETF,", " ");
            var output4 = Regex.Replace(output3, @"\bE F,", " ");
            var output5 = Regex.Replace(output4, @"\bD,", " ");
            var output6 = Regex.Replace(output5, @"\bD U,", " ");
            var output7 = Regex.Replace(output6, @"\bE U,", " ");
            var output8 = Regex.Replace(output7, @"\bETU,", " ");

            string output9 = output8.Replace(",", "");
            Import3.Text = output9;

        }

        private void SavePPCL2_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.SaveFileDialog savedialog = new Microsoft.Win32.SaveFileDialog();
            savedialog.FileName = ""; // Default file name
            savedialog.DefaultExt = ".txt"; // Default file extension
            savedialog.Filter = "Text Files (.txt)|*.txt| Rich Text files(.rtf) | *.rtf "; // Filter files by extension

            // Show open file dialog box
            Nullable<bool> result = savedialog.ShowDialog();

            // Process open file dialog box results
            if (result == true)
            {

                string saved = savedialog.FileName;
                File.WriteAllText(saved, Import3.Text);
                if (MessageBox.Show("Successfully created file! Would you like to open it now?", "Question", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    System.Diagnostics.Process.Start(saved);
                }


            }
        }

        private void clear2_Click(object sender, RoutedEventArgs e)
        {
            Import3.Text = "";
            Text2filepath.Text = "";
        }

        private void Disabled_Lines_Click(object sender, RoutedEventArgs e)
        {

            

            string find = Text1filepath.Text;
            if (find == "")
            {
                imprt();
            }

            if (find != "")
            {

                string[] textLines = File.ReadAllLines(find);



                Listoutput.ItemsSource = null;

                List<string> results = new List<string>();
                for (int i = 0; i < textLines.Length; i++)
                {
                    if (textLines[i].StartsWith(@"Panel System")
                        || textLines[i].StartsWith(@"Program Name") || textLines[i].StartsWith(@"D"))
                    {
                        results.Add(textLines[i]);

                    }
                    else if (textLines[i].StartsWith("D U"))
                    {
                        results.Add(textLines[i]);
                    }

                    else if (textLines[i].StartsWith("D F"))
                    {
                        results.Add(textLines[i]);
                    }
                }

                Listoutput.ItemsSource = results;
                string[] domains = Listoutput.Items.OfType<string>().ToArray();
                //ListBox.ItemsSource = result;

                Listoutput.ItemsSource = null;

                List<string> resultss = new List<string>();
                for (int i = 0; i < domains.Length; i++)
                {
                    if (domains[i].StartsWith("D  ")
                        && domains[i - 1].StartsWith("P"))
                    {
                        resultss.Add(Environment.NewLine);
                        resultss.Add(domains[i - 2]);
                        resultss.Add(domains[i - 1]);
                        resultss.Add(domains[i]);
                    }
                    else if (domains[i].StartsWith("D  "))
                    {
                        resultss.Add(domains[i]);
                    }

                }


                Listoutput.ItemsSource = resultss;

               


            }







        }






        private void Failed_Lines_Click(object sender, RoutedEventArgs e)
        {
            string find = Text1filepath.Text;

            if (find == "")
            {
                imprt();
            }
            if (find != "")
            {
                string[] textLines = File.ReadAllLines(find);


                Listoutput.ItemsSource = null;

                List<string> result = new List<string>();
                for (int i = 0; i < textLines.Length; i++)
                {
                    if (textLines[i].StartsWith(@"Panel System")
                        || textLines[i].StartsWith(@"Program Name") || textLines[i].StartsWith(@"ETF"))
                    {
                        result.Add(textLines[i]);

                    }
                    else if (textLines[i].StartsWith("E F"))
                    {
                        result.Add(textLines[i]);
                    }

                    else if (textLines[i].StartsWith("D F"))
                    {
                        result.Add(textLines[i]);
                    }
                }

                Listoutput.ItemsSource = result;
                string[] domains = Listoutput.Items.OfType<string>().ToArray();


                Listoutput.ItemsSource = null;

                List<string> results = new List<string>();
                for (int i = 0; i < domains.Length; i++)
                {
                    if (domains[i].StartsWith("ETF  ")
                        && domains[i - 1].StartsWith("P"))
                    {
                        results.Add(Environment.NewLine);
                        results.Add(domains[i - 2]);
                        results.Add(domains[i - 1]);
                        results.Add(domains[i]);
                    }


                    else if (domains[i].StartsWith("E F")
                        && domains[i - 1].StartsWith("P"))
                    {
                        results.Add(Environment.NewLine);
                        results.Add(domains[i - 2]);
                        results.Add(domains[i - 1]);
                        results.Add(domains[i]);
                    }


                    else if (domains[i].StartsWith("D F")
                        && domains[i - 1].StartsWith("P"))
                    {
                        results.Add(Environment.NewLine);
                        results.Add(domains[i - 2]);
                        results.Add(domains[i - 1]);
                        results.Add(domains[i]);

                    }




                    else if (domains[i].StartsWith("ETF"))
                    {
                        results.Add(domains[i]);
                    }

                    else if (domains[i].StartsWith("E F"))
                    {
                        results.Add(domains[i]);
                    }




                    else if (domains[i].StartsWith("D  F"))

                    {
                        results.Add(domains[i]);
                    }
                }
                Listoutput.ItemsSource = results;


            }





        }

        private void Unresolved_Lines_Click(object sender, RoutedEventArgs e)
        {

            string find = Text1filepath.Text;
            if (find == "")
            {
                imprt();
            }

            if (find != "")
            {

                string[] textLines = File.ReadAllLines(find);
                Listoutput.ItemsSource = null;

                List<string> result = new List<string>();
                for (int i = 0; i < textLines.Length; i++)
                {
                    if (textLines[i].StartsWith(@"Panel System")
                        || textLines[i].StartsWith(@"Program Name") || textLines[i].StartsWith("ETU"))
                    {
                        result.Add(textLines[i]);

                    }
                    else if (textLines[i].StartsWith(@"E U"))
                    {
                        result.Add(textLines[i]);
                    }

                    else if (textLines[i].StartsWith(@"D  U"))
                    {
                        result.Add(textLines[i]);
                    }
                }

                Listoutput.ItemsSource = result;
                string[] domains = Listoutput.Items.OfType<string>().ToArray();


                Listoutput.ItemsSource = null;

                List<string> results = new List<string>();
                for (int i = 0; i < domains.Length; i++)
                {
                    if (domains[i].StartsWith(@"ETU  ")
                        && domains[i - 1].StartsWith("P"))
                    {
                        results.Add(Environment.NewLine);
                        results.Add(domains[i - 2]);
                        results.Add(domains[i - 1]);
                        results.Add(domains[i]);
                    }


                    else if (domains[i].StartsWith(@"E U")
                        && domains[i - 1].StartsWith("P"))
                    {
                        results.Add(Environment.NewLine);
                        results.Add(domains[i - 2]);
                        results.Add(domains[i - 1]);
                        results.Add(domains[i]);
                    }


                    else if (domains[i].StartsWith(@"D  U")
                        && domains[i - 1].StartsWith("P"))
                    {
                        results.Add(Environment.NewLine);
                        results.Add(domains[i - 2]);
                        results.Add(domains[i - 1]);
                        results.Add(domains[i]);

                    }




                    else if (domains[i].StartsWith(@"ETU"))
                    {
                        results.Add(domains[i]);
                    }

                    else if (domains[i].StartsWith(@"E U"))
                    {
                        results.Add(domains[i]);
                    }




                    else if (domains[i].StartsWith(@"D  U"))

                    {
                        results.Add(domains[i]);
                    }
                }
                Listoutput.ItemsSource = results;




            }
        }

        private void Save_Results_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.SaveFileDialog opendialog = new Microsoft.Win32.SaveFileDialog();
            opendialog.FileName = ""; // Default file name
            opendialog.DefaultExt = ".rtf"; // Default file extension
            opendialog.Filter = "RTF files (.rtf)|*.txt"; // Filter files by extension

            // Show open file dialog box
            Nullable<bool> result = opendialog.ShowDialog();

            // Process open file dialog box results
            if (result == true)
            {
                string fileName = opendialog.FileName;

                using (var sw = new StreamWriter(fileName, false))
                    foreach (var item in Listoutput.Items)
                        sw.Write(item.ToString() + Environment.NewLine);

                if (MessageBox.Show("Successfully created file! Would you like to open it now?", "Question", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    System.Diagnostics.Process.Start(fileName);
                }


            }
        }

        private void ClearListBox_Click(object sender, RoutedEventArgs e)
        {
            Listoutput.ItemsSource = "";
            Text1filepath.Text = "";
            //Listoutput.Items.Clear();

        }

        private void Compare_Click(object sender, RoutedEventArgs e)
        {
            string s1 = Import.Text;
            string s2 = Import3.Text;

            List<string> diff;
            List<string> results = new List<string>();

            IEnumerable<string> set1 = Regex.Split(s1, @"\r?\n|\r");
            IEnumerable<string> set2 = Regex.Split(s2, @"\r?\n|\r");

            //IEnumerable<string> set1 = s1.Split(' ').Distinct()
            //IEnumerable<string> set2 = s2.Split(' ').Distinct();

            if (set2.Count() > set1.Count())
            {
                diff = set2.Except(set1).ToList();
                foreach (string d in diff)
                {
                    results.Add(d);
                    //Listoutput.Items.Add(results);
                }
            }
            else
            {
                diff = set1.Except(set2).ToList();
                foreach (string d in diff)
                {
                    results.Add(d);
                    //Listoutput.Items.Add(results);
                }
            }
            Listoutput.ItemsSource = results;

        }



        // Do the points.

        private void Getpoints()
        {
            {
                // Configure open file dialog box
                Microsoft.Win32.OpenFileDialog opendialog = new Microsoft.Win32.OpenFileDialog();
                opendialog.FileName = ""; // Default file name
                opendialog.DefaultExt = ".txt"; // Default file extension
                opendialog.Filter = "txt files (.txt)|*.txt"; // Filter files by extension

                // Show open file dialog box
                Nullable<bool> result = opendialog.ShowDialog();
                string format = "";
                // Process open file dialog box results
                if (result == true)
                {
                    string fileName = opendialog.FileName;
                    Text1filepath.Text = fileName;
                    string pointsoperator = File.ReadAllText(fileName).Trim();


                }


            }
        }

                

        private void Operator_Click(object sender, RoutedEventArgs e)
        {

           
            Pointsinoperator();

        }


        private void seeall_Click(object sender, RoutedEventArgs e)
        {

            if (Text1filepath.Text == "")
            {
                Getpoints();
               
                string points1 = Text1filepath.Text;
                string[] textLiness1 = File.ReadAllLines(points1);
                List<string> re1 = new List<string>();
              


                


                // do not include OPER in text

                foreach (string line in textLiness1)
                {

                    

                        re1.Add(line);


                    

                }

                Listoutput.ItemsSource = re1;



            }
            else
            {
                
                string points = Text1filepath.Text;
                string[] textLiness = File.ReadAllLines(points);
                List<string> re = new List<string>();



                //Listoutput.Items.Clear();


                foreach (string line in textLiness)
                {
                    {

                       

                            re.Add(line);


                        

                    }

                    Listoutput.ItemsSource = re;



                }
            }
        }


        private void Pointsinoperator()
        {

            if (Text1filepath.Text == "")
            {
                Getpoints();
                string oper1 = "OPER";
                string points1 = Text1filepath.Text;
                string[] textLiness1 = File.ReadAllLines(points1);
                List<string> re1 = new List<string>();
                Regex rgx = new Regex(@"\b.*OPER\b");


                


                // do not include OPER in text

                foreach (string line in textLiness1)
                {

                    if (line.Contains(oper1) && (line.IndexOf("OPER") > 60) || (line.Contains("BN08")) || (line.Contains("BN07")) || (line.Contains("BN06")) || (line.Contains("BN05")) || (line.Contains("BN04")) || (line.Contains("BN03")) || (line.Contains("BN02")) || (line.Contains("BN01")))

                    {

                        re1.Add(line);


                    }

                }

                Listoutput.ItemsSource = re1;



            }
            else
            {
                string oper = "OPER";
                string points = Text1filepath.Text;
                string[] textLiness = File.ReadAllLines(points);
                List<string> re = new List<string>();



                //Listoutput.Items.Clear();


                foreach (string line in textLiness)
                {
                    {

                        if (line.Contains(oper) && (line.IndexOf("OPER") > 60) || (line.Contains("BN08")) || (line.Contains("BN07")) || (line.Contains("BN06")) || (line.Contains("BN05")) || (line.Contains("BN04")) || (line.Contains("BN03")) || (line.Contains("BN02")) || (line.Contains("BN01")))

                        {

                            re.Add(line);

                        }

                    }

                    Listoutput.ItemsSource = re;

                }
            }
        }

        private void alarm_Click(object sender, RoutedEventArgs e)
        {
            if(Text1filepath.Text == "")
            {
                Getpoints();
               // Listoutput.Items.Clear();

                string text = Text1filepath.Text;
                string alar3 = @"*A3*";
                string alar1 = @"*A1*";
                string alarmm = @"*A*";



                string[] textLiness = File.ReadAllLines(text);
                List<string> re = new List<string>();


                Listoutput.Items.Clear();



                foreach (string line in textLiness)
                {
                    if ((line.Contains(alar3)) || (line.Contains(alar1)) || (line.Contains(alarmm)))
                    {
                        re.Add(line);
                        //Listoutput.Items.Add(line);
                    }



                }

                Listoutput.ItemsSource = re;


            }
            else
            {
                string text = Text1filepath.Text;
                string alar3 = @"*A3*";
                string alar1 = @"*A1*";
                string alarmm = @"*A*";



                string[] textLiness = File.ReadAllLines(text);
                List<string> re = new List<string>();



                //Listoutput.Items.Clear();


                foreach (string line in textLiness)
                {
                    if ((line.Contains(alar3)) || (line.Contains(alar1)) || (line.Contains(alarmm)))
                    {
                        re.Add(line);
                        //Listoutput.Items.Add(line);
                    }



                }
                Listoutput.ItemsSource = re;
            }
        }

        private void Failed_Click(object sender, RoutedEventArgs e)
        {
            if (Text1filepath.Text == "")
            {
                Getpoints();
                // Listoutput.Items.Clear();

                string text = Text1filepath.Text;
                string failedalarm = @"*AF*";
                string failed = @"*F*";

                string[] textLiness = File.ReadAllLines(text);
                List<string> re = new List<string>();

                foreach (string line in textLiness)
                {
                    if ((line.Contains(failedalarm)) || (line.Contains(failed)))
                    {
                        re.Add(line);
                        //Listoutput.Items.Add(line);
                    }



                }
                Listoutput.ItemsSource = re;
            }


            else
            {
                string text = Text1filepath.Text;
                string failedalarm = @"*AF*";
                string failed = @"*F*";

                string[] textLiness = File.ReadAllLines(text);
                List<string> re = new List<string>();
               

                    foreach (string line in textLiness)
                    {
                        if ((line.Contains(failedalarm)) || (line.Contains(failed)))
                        {
                            re.Add(line);
                            //Listoutput.Items.Add(line);
                        }



                    }

                    Listoutput.ItemsSource = re;
                }

                



        }

        private void disabled_Click(object sender, RoutedEventArgs e)
        {
            {
                if (Text1filepath.Text == "")
                {
                    Getpoints();
                    // Listoutput.Items.Clear();

                    string text = Text1filepath.Text;
                    string disabled = @"*O*";
                    List<string> re = new List<string>();
                    string[] textLiness = File.ReadAllLines(text);


                    foreach (string line in textLiness)
                    {
                        if ((line.Contains(disabled)))
                        {
                            re.Add(line);
                            
                        }
                        Listoutput.ItemsSource = re;


                    }


                }
                else
                {
                    string text = Text1filepath.Text;
                    string disabled = @"*O*";

                    string[] textLiness = File.ReadAllLines(text);
                    List<string> re = new List<string>();

                    foreach (string line in textLiness)
                    {
                        if ((line.Contains(disabled)))
                        {
                            re.Add(line);
                            
                        }

                        Listoutput.ItemsSource = re;

                    }







                }
            }
        }

        private void Save_Points_Click(object sender, RoutedEventArgs e)
        {
            SaveFilterelist();
        }

        private void SaveFilterelist()

        {
            Microsoft.Win32.SaveFileDialog opendialog = new Microsoft.Win32.SaveFileDialog();
            opendialog.FileName = ""; // Default file name
            opendialog.DefaultExt = ".rtf"; // Default file extension
            opendialog.Filter = "RTF files (.rtf)|*.txt"; // Filter files by extension

            // Show open file dialog box
            Nullable<bool> result = opendialog.ShowDialog();

            // Process open file dialog box results
            if (result == true)
            {
                string fileName = opendialog.FileName;

                using (var sw = new StreamWriter(fileName, false))
                    foreach (var item in Listoutput.Items)
                        sw.Write(item.ToString() + Environment.NewLine);

                if (MessageBox.Show("Successfully created file! Would you like to open it now?", "Question", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    System.Diagnostics.Process.Start(fileName);
                }


            }
        }

        private void Listoutput_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void Acitivity_Click(object sender, RoutedEventArgs e)
        {
            BASinfo bas = new BASinfo();
            bas.Show();
        }





        //private void Compare_Click(object sender, RoutedEventArgs e)
        ////{
        ////    string[] File1Lines = File.ReadAllLines(Text1filepath.Text);
        ////    string[] File2Lines = File.ReadAllLines(Text2filepath.Text);
        ////    List<string> NewLines = new List<string>();
        ////    for (int lineNo = 0; lineNo < File1Lines.Length; lineNo++)
        ////    {
        ////        if (!String.IsNullOrEmpty(File1Lines[lineNo]) && !String.IsNullOrEmpty(File2Lines[lineNo]))
        ////        {
        ////            if (String.Compare(File1Lines[lineNo], File2Lines[lineNo]) != 0)
        ////                NewLines.Add(File2Lines[lineNo]);
        ////        }
        ////        else if (!String.IsNullOrEmpty(File1Lines[lineNo]))
        ////        {
        ////        }
        ////        else
        ////        {
        ////            NewLines.Add(File2Lines[lineNo]);

        ////            Listoutput.ItemsSource = NewLines;
        ////        }
        ////    }
        ////    if (NewLines.Count > 0)
        ////    {
        ////       // File.WriteAllLines(newfilepath, NewLines);
        ////    }
        //}
    }
}
